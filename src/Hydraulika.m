R1 = 1;
S12 = 0.1;
R2 = 0.5;
h2max = 3
;
S22 = 0.1257;
h20 = 0.8067;

g=9.81;
h1max = 2*R1;

h10 = ((S22*sqrt(h20))/(S12))^2;
Q00 = S12*sqrt(2*g*h10);

A = zeros(2);
A(1) = ((-S12*sqrt(2*g)*(1/(2*sqrt(h10))))*(pi*(2*R1*h10-h10^2)))/((pi^2)*(2*R1*h10-h10^2)^2);
A(2) = ((S12*sqrt(2*g))/(pi*((R2*(1-(h20/h2max)))^2)))*(1/(2*sqrt(h10)));
A(3) = 0;
A(4) = (((-S22*sqrt(2*g))/(2*sqrt(h20)))*(pi*((R2*(1-(h20/h2max)))^2)))/((pi^2)*((R2*(1-(h20/h2max)))^4));
B = [1/(pi*(2*R1*h10-h10^2));0];

C = [1, 0; 0, 1];
D = [0; 0];

[numH, denH] = ss2tf(A, B, C, D)

T = 1;

contSys = tf(numH(2,:), denH)
discSys = c2d(contSys, T)
[numDH, denDH] = tfdata(discSys, 'v')
