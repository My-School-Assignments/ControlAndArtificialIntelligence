function [ kx, kw, nD, dD, Ad, bD, Cd ] = modal( num, den, poles, T )
%MODAL Summary of this function goes here
%   Detailed explanation goes here
% sys = tf(num, den);
% sysD = c2d(sys, 1)
% [nD, dD] = tfdata(sysD, 'v')

nD = num;
dD = den;

nD = nD(cumsum(nD,2) > 0);

Ad = [0 1;
      -dD(3), -dD(2)];
bD = [0;1];

Cd = [nD(2), nD(1)];

q1 = -(poles(1) + poles(2));
q2 = poles(1)*poles(2);

k2 = q2 - dD(3);
k1 = q1 - dD(2);

kx = [k2 k1];

Ac = (Ad - bD*kx)

I = eye(2);

kw = 1 / (Cd*inv(I - Ac)*bD);

end

