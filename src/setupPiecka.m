num = 0.1
den = [1 1 0.2]

regType = input('Regulator type (0:P, 1:PI, 2:PD, 3:PID): ');
znMethod = input('Method for Zieger-Nichols (0:Graphical, 1:Frequencies): ');
ssMethod = input('Method for Standard Shapes (0:G-L, 1:Butterworth): ');
T = input('Time period: ');
TPP = input('Time period PolePlacement: ');
poles = input('Poles: ');
TDB = input('Time period DeadBeat: ');
limitDB = input('Limit DeadBead (0 no limit): ');

[PNas, INas, DNas] = naslin(num, den, regType);
[q0N, q1N, q2N] = PID2PSD(PNas, INas, DNas, T);

[PO, IO, DO] = optimal(num, den, regType);
[q0O, q1O, q2O] = PID2PSD(PO, IO, DO, T);

if(ssMethod == 0)
    [PS, IS, DS] = GL(num, den, regType);
    [q0S, q1S, q2S] = PID2PSD(PS, IS, DS, T);
elseif(ssMethod == 1)
    [PS, IS, DS] = Butt(num, den, regType);
    [q0S, q1S, q2S] = PID2PSD(PS, IS, DS, T);
end

if(znMethod == 1)
    [PZ, IZ, DZ] = ZiegerNichols(num, den, regType, 'f');
    [q0Z, q1Z, q2Z] = PID2PSD(PZ, IZ, DZ, T);
elseif(znMethod == 0)
    [PZ, IZ, DZ] = ZiegerNichols(num, den, regType, 'g');
    [q0Z, q1Z, q2Z] = PID2PSD(PZ, IZ, DZ, T);
end

contSys = tf(num, den);
discSys = c2d(contSys, T);
[numD, denD] = tfdata(discSys, 'v');

contSys = tf(num, den);
discSys = c2d(contSys, TPP);
[numDPP, denDPP] = tfdata(discSys, 'v');

[numPP, denPP] = PolePlacement(numDPP, denDPP, poles, TPP);

contSys = tf(num, den);
discSys = c2d(contSys, TDB);
[numDDB, denDDB] = tfdata(discSys, 'v');

if(limitDB ==0)
    [DBN, DBD] = deadbeat(numDDB, denDDB);
else
    [DBN, DBD] = deadbeatLimited(numDDB, denDDB, limitDB);
end