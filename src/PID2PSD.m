function [ q0, q1, q2 ] = PID2PSD( P, I, D, T )
%PID2PSD Converts PID to PSD with given T period

if(P == 0 && I == 0 && D == 0)
    q0 = 0;
    q1 = 0;
    q2 = 0;
    return;
end

K = P;
Ti = K / I;
Td = D / K;

% q0 = K*(1 + (Td/T));
% q1 = -K*(1 + 2*(Td/T) - (T/Ti));
% q2 = K*(Td/T);

q0 = K*(1 + (T/(2*Ti)) + (Td/T));
q1 = -K*(1 + 2*(Td/T) - (T/(2*Ti)));
q2 = K*(Td/T);
end

