function [ P, I, D ] = ZiegerNichols( num, den, type, method )
%ZIEGERNICHOLS Synthesis method
% nom - nominator in TF
% den - denominator in TF
% type - 0:P; 1:PI; 2:PD; 3:PID
% method - 'g':Graphic; 'f':Frequencies
% return - P, I, D

P = 0;
I = 0;
D = 0; 

if(method == 'f')
    if(length(den) <= 3)     
        fprintf('Cannot calculate regulator using Frequencies for system of lower order than 3')
        return;
    end
    
    sys = tf(num, den);
    [Gm, Pm, Wgm, Wpm] = margin(sys);
    Tk = 2*pi / Wgm;
    r0K = Gm;
    
    if(type == 0)
        P = 0.5 * r0K;
        return;
    elseif(type == 1)
        P = 0.45 * r0K;
        I = P / (0.85*Tk);
        return;
    elseif(type == 2)
        P = 0.4 * r0K;
        D = 0.5 * Tk;
        return;
    elseif(type == 3)
        P = 0.6 * r0K;
        I = P / (0.5*Tk);
        D = 0.125 * Tk * r0K;
        return;
    end
    
elseif(method == 'g')
    
    if(length(num) > 1)
        fprintf('Not Supported!')
        return;
    end     
    
    num = num / den(end);
    den = den / den(end);
    
    sys = tf(num, den);
    [y, t] = step(sys);
    yStab = num;
    
    d1y = gradient(y,t);  
    d2y = gradient(d1y,t);   
    
    t_infl = interp1(d1y, t, max(d1y));  
    y_infl = interp1(t, y, t_infl);  
    slope  = interp1(t, d1y, t_infl);   
    intcpt = y_infl - slope*t_infl;  
    
    Tu = -intcpt / slope;
    Tn = (yStab - intcpt) / slope;
    Tn = Tn - Tu;   
    
    if(type == 0)
        P = Tn / (num * Tu);
        return;
    elseif(type == 1)
        P = (0.9 * Tn) / (num * Tu);
        I = 3.5 * Tu;
        return;
    elseif(type == 2)
        P = (1.2 * Tn) / (num * Tu);
        D = 0.25 * Tu;
        return;
    elseif(type == 3)
        P = (1.25 * Tn) / (num * Tu);
        I = 2 * Tu;
        D = 0.05 * Tu;
        return;
    end
end

end

