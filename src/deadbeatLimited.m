function [ DBNom, DBDen ] = deadbeatLimited( nom, den, lim )
%DEADBEAT Regulator


nom = nom(cumsum(nom,2) > 0);
q0 = lim;
den = [den, 0];
sNum = sum(nom);
dDen = diff(den);
dDen = dDen * q0;
right = (den ./ sNum);
Q = dDen + right(1:end-1);
Q = [q0, Q];
den = den(1:end-1);

nom = [zeros(1, length(den) - length(nom)), nom];
num = [nom, 0];
dNum = diff(num);
dNum = dNum * q0;
right = (num ./ sNum);
P = dNum + right(1:end-1);
P = [1, -P];

DBNom = Q;
DBDen = P;
end

