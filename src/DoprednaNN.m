data_num_Total = size(outDataNN, 1);
data_num_Train = round(data_num_Total*0.75);
data_num_Test = data_num_Total - data_num_Train;

InTrenFF = inDataNN(1:data_num_Train, :);
InTestFF = inDataNN(data_num_Train:data_num_Total, :);
OutTrenFF = outDataNN(1:data_num_Train, :);
OutTestFF = outDataNN(data_num_Train:data_num_Total, :);

plot(1:length(OutTrenFF), OutTrenFF(:, 2), 'b', (1:length(OutTestFF)) + length(OutTrenFF), OutTestFF(:, 2), 'r');
title('Výstup: tren - blue, test - red');

% Klasicky dopredny model
net_FF = feedforwardnet(10);
net_FF = train(net_FF,InTrenFF', OutTrenFF');

% Testovanie Dopredneho modelu
yTest = net_FF(InTestFF');
e = yTest - OutTestFF';
figure;
plot(e(2,:)');

% NARX model
y = con2seq(OutTrenFF');
u = con2seq(InTrenFF');

d1 = [1:2];
d2 = [1:2];
net = narxnet(d1,d2,10);
narx_net.divideFcn = '';
narx_net.trainParam.min_grad = 1e-10;
[p,Pi,Ai,t] = preparets(net,u,{},y);

net = train(net,p,t,Pi);

% Testovanie NARX modelu
yT = con2seq(OutTestFF');
uT = con2seq(InTestFF');
[p2,Pi2,Ai2,t2] = preparets(net,uT,{},yT);
yTest = sim(net, p2,Pi2);
e = cell2mat(yTest)-cell2mat(t2);
figure;
plot(e(2,:)')

% Narx model v uzavretej slucke
net_closed = closeloop(net);
%view(net_closed)

gensim(net_closed,T); % vytvorenie modelu pre simulink
gensim(net_FF,T); % vytvorenie modelu pre simulink

