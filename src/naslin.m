function [ P, I, D ] = naslin( nom, den, type )
%NASLIN Synthesis method
% nom - nominator in TF
% den - denominator in TF
% type - 0:P; 1:PI; 2:PD; 3:PID
% return - P, I, D

if(length(nom) > 1)
    P = 0;
    I = 0;
    D = 0;
    fprintf('Not supported.\n')
    return;
end

if(length(den) < 3)
    P = 0;
    I = 0;
    D = 0;
    fprintf('Cannot calculate regulator for system of lower order than 2')
    return;
end

if(length(den) == 3 && type == 3)
    P = 0;
    I = 0;
    D = 0;
    fprintf('Cannot calculate PID for 2nd order system using Naslin method.\n')
    return;
end

alpha = 2;
den = den / nom;
den = fliplr(den);

if(type == 0)
    P = (den(2)*den(2) - alpha*den(3)*den(1)) / (alpha*den(3));
    I = 0;
    D = 0;
    
    if(isnan(P) || isnan(I) || isnan(D) || isinf(P) || isinf(I) || isinf(D))
        P = 0;
        I = 0;
        D = 0;
    end
    
    return;
end

if(type == 1)
    P = (den(2)*den(2) - alpha*den(3)*den(1)) / (alpha*den(3));
    I = ((den(1) + P) * (den(1) + P)) / (alpha*den(2));
    D = 0;
    
    if(isnan(P) || isnan(I) || isnan(D) || isinf(P) || isinf(I) || isinf(D))
        P = 0;
        I = 0;
        D = 0;
    end
    return;
end

if(type == 2)
    if(length(den) == 3)
        D = input('For this setup you need to provide r1 (D of PD) value:');
        P = (((D + den(2)) * (D + den(2))) - (alpha*den(1)*den(3))) / (alpha*den(1));
        I = 0;
    
        if(isnan(P) || isnan(I) || isnan(D) || isinf(P) || isinf(I) || isinf(D))
            P = 0;
            I = 0;
            D = 0;
        end
        return;
    end
    I = 0;
    D = ((den(3)*den(3)) - (alpha*den(3)*den(1))) / (alpha*den(3));
    P = (((D + den(2)) * (D + den(2))) - (alpha*den(1)*den(3))) / (alpha*den(1));
    
    if(isnan(P) || isnan(I) || isnan(D) || isinf(P) || isinf(I) || isinf(D))
        P = 0;
        I = 0;
        D = 0;
    end
    return;
end

if(type == 3)
    D = ((den(3)*den(3)) - (alpha*den(4)*den(2))) / (alpha * den(4));
    P = (((D+den(2)) * (D+den(2))) - (alpha*den(3)*den(1))) / (alpha*den(3));
    I = ((den(1) + P) * (den(1) + P)) / (alpha*(den(2) + D));
    
    if(isnan(P) || isnan(I) || isnan(D) || isinf(P) || isinf(I) || isinf(D))
        P = 0;
        I = 0;
        D = 0;
    end
    return;
end
end

