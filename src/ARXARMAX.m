model = 'DataGenerateModel';
load_system(model)
sim(model)

t = tt(151/T:end);
h2 = h2Data(151/T:end);
in = inData(151/T:end);

plot(t, in, 'b');
title('Vstup do systému U');


h2 = h2 - h20;
in = in - Q00;

mid = length(h2) / 2;
h2tren = h2(1:mid);
h2test = h2(mid+1:end);
intren = in(1:mid);
intest = in(mid+1:end);

plot(t(1:mid), h2tren, 'b', t(mid+1:end), h2test, 'r');
title('Výstup: tren - blue, test - red');

inDatad = inData - Q00; % Normalize
h2Datad = h2Data - h20; % Normalize

th1 = arx([h2tren intren],[2 2 1]);
sysdarx = tf(th1.B,th1.A,T, 'Variable','z^-1')
[numARX, denARX] = tfdata(sysdarx, 'v');
h2arx = idsim(inDatad,th1);

th2 = armax([h2tren intren],[2 2 2 1]);
sysdarmax = tf(th2.B,th2.A,T, 'Variable','z^-1')
[numARMAX, denARMAX] = tfdata(sysdarmax, 'v');
h2armax = idsim(inDatad, th2);

plot(tt, h2arx', 'b', tt, h2Datad', 'r');
title('ARX (blue), Original (red)');
figure;
plot(tt', h2armax, 'b', tt', h2Datad, 'r');
title('ARMAX (blue), Original (red)');

arxMSE = mean((h2Datad - h2arx).^2)
armaxMSE = mean((h2Datad - h2armax).^2)
