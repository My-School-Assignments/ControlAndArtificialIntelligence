function [ P, I, D ] = GL( num, den, type )
%GL Synthesis method
% nom - nominator in TF
% den - denominator in TF
% type - 0:P; 1:PI; 2:PD; 3:PID
% return - P, I, D

P = 0;
I = 0;
D = 0;

if(length(num) > 1)
    fprintf('Not supported.\n')
    return;
end

if(type == 0 || type == 2)    
    if(length(den) == 3)
        [P, I, D] = StandardShapes(num, den, [1 1.4 1], type);
        return;
    elseif(length(den) == 4)
        [P, I, D] = StandardShapes(num, den, [1 1.75 2.15 1], type);
        return;
    elseif(length(den) == 5)
        [P, I, D] = StandardShapes(num, den, [1 2.1 3.4 2.7 1], type);
        return;
    elseif(length(den) == 6)
        [P, I, D] = StandardShapes(num, den, [1 2.8 5 5.5 3.4 1], type);
        return;
    end
else   
    if(length(den) == 3)
        [P, I, D] = StandardShapes(num, den, [1 1.75 2.15 1], type);
        return;
    elseif(length(den) == 4)
        [P, I, D] = StandardShapes(num, den, [1 2.1 3.4 2.7 1], type);
        return;
    elseif(length(den) == 5)
        [P, I, D] = StandardShapes(num, den, [1 2.8 5 5.5 3.4 1], type);
        return;
    end
end
end

