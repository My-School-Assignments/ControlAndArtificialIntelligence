function [ DBNom, DBDen ] = deadbeat( nom, den )
%DEADBEAT Regulator

%nom = nom(cumsum(nom,2) > 0);
q0 = 1 / sum(nom);
Q = q0 * den;
P = q0 * nom;

DBNom = Q;
DBDen = [1 -P];
end

