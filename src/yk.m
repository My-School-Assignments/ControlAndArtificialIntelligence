function eq = yk( numS, denS, numR, denR )
%yk Computes discrete y(k) in 
% time for discrete closed-loop system

if(numel(numR) > numel(denR))
    denR(numel(numR)) = 0;
end

if(numel(numR) < numel(denR))
    numR(numel(denR)) = 0;
end

numURO = conv(numS, numR);
denURO1 = conv(numS, numR);
denURO2 = conv(denS, denR);

denURO = denURO1 + denURO2;

eq = 'y(k) = ';

for i = 2:numel(denURO)
    if -denURO(i) > 0
        eq = strcat(eq, '+ ');
    end
    eq = strcat(eq, num2str(-denURO(i), '%.3f'));
    eq = strcat(eq, 'y(k - ');
    eq = strcat(eq, num2str(i - 1, '%d'));
    eq = strcat(eq, ') ');
end

for i = 1:numel(numURO)
    if numURO(i) > 0
        eq = strcat(eq, ' + ');
    end
    eq = strcat(eq, num2str(numURO(i), ' %.3f'));
    eq = strcat(eq, 'w(k - ');
    eq = strcat(eq, num2str(i - 1, ' %d'));
    eq = strcat(eq, ') ');
end

end

