data_num_Inv_Total = size(outDataInvNN, 1);
data_num_Inv_Train = round(data_num_Total*0.75);
data_num_Inv_Test = data_num_Total - data_num_Train;

InTrenInvFF = inDataInvNN(1:data_num_Inv_Train, :);
InTestInvFF = inDataInvNN(data_num_Inv_Train:data_num_Inv_Total, :);
OutTrenInvFF = outDataInvNN(1:data_num_Inv_Train, :);
OutTestInvFF = outDataInvNN(data_num_Inv_Train:data_num_Inv_Total, :);

plot(1:length(OutTrenInvFF), OutTrenInvFF(:, 1), 'b', (1:length(OutTestInvFF)) + length(OutTrenInvFF), OutTestInvFF(:, 1), 'r');
title('Výstup: tren - blue, test - red');

netINV = feedforwardnet(4);
netINV = train(netINV,InTrenInvFF',OutTrenInvFF');

% Testovanie Dopredneho modelu
yTest = netINV(InTestInvFF');
e = yTest - OutTestInvFF';
figure;
plot(e');

gensim(netINV,T); % vytvorenie modelu pre simulink