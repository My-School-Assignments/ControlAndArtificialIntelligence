function [ numP, denP ] = PolePlacement( num, den, poles, T )
%POLEPLACEMENT Synthesis method
% nom - nominator in TF
% den - denominator in TF
% poles - poles to use
% T - step period
% return - numP, denP

%p = exp(poles*T);
p = poles;
num = num(cumsum(num,2) > 0);
b = num;
a = den;

z1 = -sum(p);
z2 = p(1)*p(2) + p(1)*p(3) + p(1)*p(4) + p(2)*p(3) + p(2)*p(4) + p(3)*p(4);
z3 = -(p(1)*p(2)*p(3) + p(1)*p(2)*p(4) + p(1)*p(3)*p(4) + p(2)*p(4)*p(3));
z4 = p(1)*p(2)*p(3)*p(4);

A = [b(1), 0,    0,    -1;
     b(2), b(1), 0,    (-a(2)+1);
     0,    b(2), b(1), (-a(3)+a(2));
     0,    0,    b(2), a(3)];
 
B = [(z1 + 1 - a(2));...
     (z2 + a(2) - a(3));...
     (z3 + a(3));
     z4];

q = A \ B;

numP = [q(1), q(2), q(3)];
denP = [1, -(1+q(4)), q(4)];

end

