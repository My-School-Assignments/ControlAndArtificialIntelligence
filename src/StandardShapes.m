function [ P, I, D ] = StandardShapes( num, den, shape, type )
%STANDARDSHAPES Synthesis method
% nom - nominator in TF
% den - denominator in TF
% shape - plynom to compare with CHR
% type - 0:P; 1:PI; 2:PD; 3:PID
% return - P, I, D

P = 0;
I = 0;
D = 0;

if(length(num) > 1)
    fprintf('Not supported.\n')
    return;
end

if(length(den) == 3 && type >= 2)
    fprintf('Cannot calculate PD or PID for 2nd order system.\n')
    return;
end
    
    
if(shape(1) ~= 1)
    shape = shape / shape(1);
end

num = num / den(1);
den = den / den(1);
omega = den(2) / shape(2);

if(length(shape) > 2)
    for i = 3:(length(shape))
        shape(i) = shape(i) * (omega ^ (i - 1));
    end
end

den = fliplr(den);
shape = fliplr(shape);

if(type == 0)
    P = (shape(1) - den(1)) / num;
    I = 0;
    D = 0;
    
    if(isnan(P) || isnan(I) || isnan(D) || isinf(P) || isinf(I) || isinf(D))
        P = 0;
        I = 0;
        D = 0;
    end
    
    return;
end

if(type == 1)
    P = (shape(2) - den(1)) / num;
    I = shape(1) / num;
    D = 0;
    
    if(isnan(P) || isnan(I) || isnan(D) || isinf(P) || isinf(I) || isinf(D))
        P = 0;
        I = 0;
        D = 0;
    end
    return;
end

if(type == 2)
    I = 0;
    D = (shape(2) - den(2)) / num;
    P = (shape(1) - den(1)) / num;
    
    if(isnan(P) || isnan(I) || isnan(D) || isinf(P) || isinf(I) || isinf(D))
        P = 0;
        I = 0;
        D = 0;
    end
    return;
end

if(type == 3)
    D = (shape(3) - den(2)) / num;
    P = (shape(2) - den(1)) / num;
    I = shape(1) / num;
    
    if(isnan(P) || isnan(I) || isnan(D) || isinf(P) || isinf(I) || isinf(D))
        P = 0;
        I = 0;
        D = 0;
    end
    return;
end

end

