function [ P, I, D ] = optimal( nom, den, type )
%OPTIMAL MODUL Synthesis method
% nom - nominator in TF
% den - denominator in TF
% type - 0:P; 1:PI; 2:PD; 3:PID
% return - P, I, D

if(length(nom) > 1)
    P = 0;
    I = 0;
    D = 0;
    fprintf('Not supported.\n')
    return;
end

den = fliplr(den);
nom = nom / den(1);
den = den ./ den(1);

if(length(den) < 6)
    den(6) = 0;
end

if(type == 0)    
    P = 0;
    I = 0;
    D = 0;
    fprintf('Not supported.\n')
    return;
end

if(type == 1)
    A = [den(2), -1;...
         den(4), -den(3)];
    B = [1;...
         -(den(2)*den(2)) + (2*den(3))] ./ (2*nom);
    
    res = A \ B; 
    
    P = res(2);
    I = res(1);    
    D = 0;
    
    if(isnan(P) || isnan(I) || isnan(D) || isinf(P) || isinf(I) || isinf(D))
        P = 0;
        I = 0;
        D = 0;
    end
    
    return;
end

if(type == 2)
    A = [-den(3), den(2);...
         -den(5), den(4)];
    B = [-(den(2)*den(2)) + (2*den(3));...
         (den(3)*den(3)) - (2*den(2)*den(4)) + (2*den(5))] ./ (2*nom);
    
    res = A \ B; 
    
    I = 0;
    D = res(2);
    P = res(1);
    
    if(isnan(P) || isnan(I) || isnan(D) || isinf(P) || isinf(I) || isinf(D))
        P = 0;
        I = 0;
        D = 0;
    end
    
    return;
end

if(type == 3)
    A = [den(2), -1, 0;...
         den(4), -den(3), den(2);...
         den(6), -den(5), den(4)];
    B = [1;...
         -(den(2)*den(2)) + (2*den(3));...
         (den(3)*den(3)) - (2*den(2)*den(4)) + (2*den(5))] ./ (2*nom);
    
    res = A \ B; 
    
    D = res(3);
    P = res(2);
    I = res(1);
    
    if(isnan(P) || isnan(I) || isnan(D) || isinf(P) || isinf(I) || isinf(D))
        P = 0;
        I = 0;
        D = 0;
    end
    
    return;
end

end

