# ControlAndArtificialIntelligence
Set of MATLAB scripts for Control and Artificial Intelligence Class at TUKE.

Contains scripts for computing PID using standard synthesis methods, some discrete controllers and also some of the adaptive algorithms used in control theory.

---
**© Dominik Horňák**